<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
$router->get('/',  function () use ($router) {
    return $router->app->version();
});
$router->get('/get_ticket_detail', 'Datamart\DatamartController@get_ticket_detail');
// $router->get('/get_ticket_detail', 'ExampleController@index');
$router->post('/post_ticket', 'Datamart\DatamartController@post_ticket');
$router->post('/update_ticket', 'ExampleController@index');

// Route::get('/download_excell/{id}/{token}',  'ExampleController@index');
// Route::post('/testing',  'Dashboard\DashboardController@testing');