<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Datamasters extends Model

{
    protected $fillable = [ 
        'subdir',
        'customer',
        'apiKey',
        'sourceTicketId',
        'priority',
        'ticketDate',
        'channel',
        'category',
        'status',
        'caseOwner',
        'contactName',
        'contactIdentification',
        'subject',
        'description',
        'webEmail',
    ];
}